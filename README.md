# cutthecord

Discord Android app patches.

Automatically patched binaries are available on [distok.a3.pm/cutthecord](https://distok.a3.pm/cutthecord).

## Toolchain setup

- Get apktool
- Get a keystore, see [here](https://stackoverflow.com/a/14994354/3286892), step 1.
- Get 72x72 copies of latest version of mutant standard emojis with codepoints. I have a zip [here](https://mutant.lavatech.top/72x72.zip).
- Extract the emojis you got somewhere.
- Clone this repo somewhere, edit `emojireplace.py` and set the `extracted_mutstd_path` folder to the folder you just extracted emojis to.

## Building an patched discord app

- Get a Discord apk (*cough* [aptoide](https://ws75.aptoide.com/api/7/app/getMeta?package_name=com.discord)).
- Extract it with apktool (`apktool d <apk path>`)
- Get all the necessary patches for that version. Necessary patches are not available for all versions and are only required to get some versions to pack together correctly.
- Get optional patches you want for your version. If the patch you want isn't available for your version, you'll have to port them yourself.
- Apply the patches (`patch -p1 < <patch name>`).
- Edit `emojireplace.py` to point to extracted discord folder (`extracted_discord_path`), and apply emoji patches (`python3 emojireplace.py`)
- Build the new APK (`apktool b com.discord-831`)
- Sign the new APK (`jarsigner -keystore <keystore path> <foldername>/dist/<foldername>.apk <alias>`)
- Get your new APK from `<foldername>/dist/<foldername>.apk`, install and enjoy!

## Building patches

`diff -crB -x "dist" -x "res" -x "build" CleanFolder PatchedFolder > patchname.patch`

## Disclaimer about emojos

This [thing] uses Mutant Standard emoji (https://mutant.tech), which are licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (https://creativecommons.org/licenses/by-nc-sa/4.0/).

