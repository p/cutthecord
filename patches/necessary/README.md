## DisTok CutTheCord: Necessary Patches

These patches are used in cases where discord fucks up and repacks fail.

If the version you're using is in the following list, please make sure to use the relevant patch file before repacking.

#### Available and tested on:
- 8.3.1
- 8.3.2

