## DisTok CutTheCord: Disable Mobile Indicator Patch

This patch replaces the browser name from "Discord Android" to "Discord Client", disabling the Mobile Indicator which is a privacy violation without an explicit opt-out option.

However, this causes some issues. See the `Bugs / Side effects` list below.

#### Bugs / Side effects
- CRITICAL: Push notifications are broken

#### Available and tested on:
- 8.3.0
- 8.3.1

