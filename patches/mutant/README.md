## DisTok CutTheCord: Mutant Standard Emoji Patch

This patch replaces internal emoji list with the custom emojis of mutant standard.

You'll need to pack in the right images to the apk. See README.md at the root of the repo for more information.

#### Bugs / Side effects
- Not all emojis are replaced
- Not all custom mutstd emojis are added
- Custom mutstd emojis won't be visible for users who aren't using a patch like this.

#### Available and tested on:
- 8.3.1
- 8.3.2

#### Disclaimer

This [thing] uses Mutant Standard emoji (https://mutant.tech), which are licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (https://creativecommons.org/licenses/by-nc-sa/4.0/).

