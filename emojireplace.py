#!/bin/env python3
import os
import shutil

# REPLACE THESE LINES
extracted_discord_path = "/tmp/cutthecord/discord"
extracted_mutstd_path = "/var/www/mutant/72x72"

# Add your custom emojis here
# with "mutstd filename": "discord filename".
# You'll need to write a patch for `assets/data/emojis.json` too.
custom_emojis = {"1f4af-200d-1f308.png": "emoji_1f4af_1f308.png",
                 "10169b-200d-1f308.png": "emoji_10169b_1f308.png",
                 "1f9d1-200d-2708-fe0f.png": "emoji_1f9d1_2708.png",
                 "1f9d1-200d-2695-fe0f.png": "emoji_1f9d1_2695.png",
                 "1f9d1-200d-1f962.png": "emoji_1f9d1_1f962.png",
                 "1f9d1-200d-1f680.png": "emoji_1f9d1_1f680.png",
                 "1f9d1-200d-1f52c.png": "emoji_1f9d1_1f52c.png",
                 "1f9d1-200d-1f527.png": "emoji_1f9d1_1f527.png",
                 "1f9d1-200d-1f4bb.png": "emoji_1f9d1_1f4bb.png",
                 "1f9d1-200d-1f373.png": "emoji_1f9d1_1f373.png",
                 "10169a.png": "emoji_10169a.png",
                 "26b2-fe0f.png": "emoji_26b2.png",
                 "26a8-fe0f.png": "emoji_26a8.png",
                 "26a7-fe0f.png": "emoji_26a7.png",
                 "26a5-fe0f.png": "emoji_26a5.png",
                 "26a4-fe0f.png": "emoji_26a4.png",
                 "26a3-fe0f.png": "emoji_26a3.png",
                 "26a2-fe0f.png": "emoji_26a2.png",
                 "2674-fe0f.png": "emoji_2674.png",
                 "2642-fe0f.png": "emoji_2642.png",
                 "2640-fe0f.png": "emoji_2640.png",
                 "1f9e1.png": "emoji_1f9e1.png",
                 "101685.png": "emoji_101685.png",
                 "101684.png": "emoji_101684.png",
                 "101683.png": "emoji_101683.png",
                 "101682.png": "emoji_101682.png",
                 "1f9fb.png": "emoji_1f9fb.png",
                 "1f9f1.png": "emoji_1f9f1.png",
                 "1f9e8.png": "emoji_1f9e8.png",
                 "1f9b4.png": "emoji_1f9b4.png",
                 "101696.png": "emoji_101696.png",
                 "101695.png": "emoji_101695.png",
                 "101694.png": "emoji_101694.png",
                 "101693.png": "emoji_101693.png",
                 "101692.png": "emoji_101692.png",
                 "10169b.png": "emoji_10169b.png",
                 "101698.png": "emoji_101698.png",
                 "101699.png": "emoji_101699.png",
                 "1f9dd.png": "emoji_1f9dd.png",
                 "1f99d.png": "emoji_1f99d.png",
                 "1f99c.png": "emoji_1f99c.png",
                 "1f99a.png": "emoji_1f99a.png",
                 "101666.png": "emoji_101666.png",
                 "1f9d0.png": "emoji_1f9d0.png",
                 "1f97a.png": "emoji_1f97a.png",
                 "1f976.png": "emoji_1f976.png",
                 "1f975.png": "emoji_1f975.png",
                 "1f974.png": "emoji_1f974.png",
                 "1f973.png": "emoji_1f973.png",
                 "1f970.png": "emoji_1f970.png",
                 "1f92f.png": "emoji_1f92f.png",
                 "1f92e.png": "emoji_1f92e.png",
                 "1f92d.png": "emoji_1f92d.png",
                 "1f92c.png": "emoji_1f92c.png",
                 "1f92b.png": "emoji_1f92b.png",
                 "1f92a.png": "emoji_1f92a.png",
                 "1f929.png": "emoji_1f929.png",
                 "1f928.png": "emoji_1f928.png",
                 "1f575-10162b.png": "emoji_1f575_10162b.png",
                 "1f486-10162b.png": "emoji_1f486_10162b.png",
                 "1f481-10162b.png": "emoji_1f481_10162b.png",
                 "101690.png": "emoji_101690.png",
                 "101697.png": "emoji_101697.png",
                 "1f9dc.png": "emoji_1f9dc.png",
                 "1f3f4-fe0f.png": "emoji_1f3f4_fe0f.png",
                 "101681.png": "emoji_101681.png",
                 "101680.png": "emoji_101680.png",
                 "10167f.png": "emoji_10167f.png",
                 "10167e.png": "emoji_10167e.png",
                 "10167d.png": "emoji_10167d.png",
                 "10167c.png": "emoji_10167c.png",
                 "10167b.png": "emoji_10167b.png",
                 "10167a.png": "emoji_10167a.png",
                 "101679.png": "emoji_101679.png",
                 "101678.png": "emoji_101678.png",
                 "101677.png": "emoji_101677.png",
                 "101676.png": "emoji_101676.png",
                 "101675.png": "emoji_101675.png",
                 "101674.png": "emoji_101674.png",
                 "101673.png": "emoji_101673.png",
                 "101672.png": "emoji_101672.png",
                 "101671.png": "emoji_101671.png",
                 "101670.png": "emoji_101670.png",
                 "101686.png": "emoji_101686.png",
                 "101691.png": "emoji_101691.png"}


def clean_emoji_name(name):
    name = name.lower().replace("_", "-")\
        .replace("emoji-", "").replace("-fe0f", "")
    return name


discord_emoji_path = os.path.join(extracted_discord_path, "res", "raw")
# Get file listings in relevant folders
discord_emojis = os.listdir(discord_emoji_path)
mutstd_emojis = os.listdir(extracted_mutstd_path)

# Clean names of mutantstd emojis so thar we can compare them
# to clean discord emojis later
clean_mutstd_emojis = {clean_emoji_name(emoji): emoji for
                       emoji in mutstd_emojis}

replace_counter = 0

# Go through each discord emoji, and clean their names
for emoji in discord_emojis:
    clean_discord_emoji = clean_emoji_name(emoji)

    # Check if said clean name of emoji is in clean mutstd list
    if clean_discord_emoji in clean_mutstd_emojis:
        # Get full unclean filename of mutantstd emoji, generate relevant paths
        full_mutstd_name = clean_mutstd_emojis[clean_discord_emoji]
        full_mutstd_path = os.path.join(extracted_mutstd_path, full_mutstd_name)
        full_discord_path = os.path.join(discord_emoji_path, emoji)

        # Copy and overwrite the discord emojis with the mutantstd alternatives
        shutil.copyfile(full_mutstd_path, full_discord_path)

        print("Replaced {} emoji.".format(emoji))
        replace_counter += 1

for custom_emoji in custom_emojis:
    discord_emoji_name = custom_emojis[custom_emoji]
    full_mutstd_path = os.path.join(extracted_mutstd_path, custom_emoji)
    full_discord_path = os.path.join(discord_emoji_path, discord_emoji_name)
    shutil.copyfile(full_mutstd_path, full_discord_path)
    print("Added custom {} emoji.".format(discord_emoji_name))
    replace_counter += 1

print("Done, {} emojis replaced.".format(replace_counter))
